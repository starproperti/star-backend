<?php
    session_start();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>StarProperti Admin - Dashboard</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="assets/scss/style.css">
    <link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>

    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <?php
            include 'sidebar.php';
        ?>
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->

    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">
            <?php 
                include 'navbar.php';
            ?>
        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Data Properti</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Properti</a></li>
                            <li class="active">Data Properti</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Properti (Pending)</strong>
                        </div>
                        <div class="card-body table-responsive">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th><center>No.</center></th>
                                        <th><center>Nama Property</center></th>
                                        <th><center>Alamat Property</center></th>
                                        <th><center>Luas Property</center></th>
                                        <th><center>Harga Property</center></th>
                                        <th><center>Opsi</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        include 'proses/databaseUtility.php';
                                        $no = 1;
                                        $QueryS = mysql_query("SELECT * FROM data_property WHERE id_status_persetujuan_property = '3' ORDER BY id ");
                                        while ($arrayWait = mysql_fetch_array($QueryS)) {
                                    ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$arrayWait['nama_property']?></td>
                                        <td><?=$arrayWait['alamat_property']?></td>
                                        <td><?=$arrayWait['luas_property']?>m<sup>2</sup></td>
                                        <td>Rp.<?=$arrayWait['harga_property']?></td>
                                        <td><center>
                                            <a href="tanggapi_properti.php?id=<?=$arrayWait['id']?>">
                                                <button type="button" class="btn btn-info"><i class="fa fa-pencil"></i>&nbsp;Tanggapi</button>
                                            </a>
                                        </center></td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                          </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Properti (Disetujui) </strong>
                        </div>
                        <div class="card-body table-responsive">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th><center>No.</center></th>
                                        <th><center>Nama Property</center></th>
                                        <th><center>Alamat Property</center></th>
                                        <th><center>Harga Property</center></th>
                                        <th><center>Status Property</center></th>
                                        <th><center>Opsi</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        $QueryS = mysql_query("SELECT * FROM data_property WHERE id_status_persetujuan_property = 1 Order BY id");
                                        while ($arraySetuju = mysql_fetch_array($QueryS)) {
                                    ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$arraySetuju['nama_property']?></td>
                                        <td><?=$arraySetuju['alamat_property']?></td>
                                        <td><?=$arraySetuju['harga_property']?></td>
                                        <td>
                                            <?php
                                                $QuerySe = mysql_query("SELECT * FROM status_property");
                                                while ($arrayStatus = mysql_fetch_array($QuerySe)) {
                                                    if ($arrayStatus['id']==$arraySetuju['id_status_property']) {
                                                        echo $arrayStatus['status'];
                                                    }
                                                }
                                            ?>        
                                        </td>
                                        <td><center>
                                            <a href="edit_properti.php?id=<?=$arraySetuju['id']?>">
                                                <button type="button" class="btn btn-info"><i class="fa fa-pencil"></i>&nbsp;Edit</button>
                                            </a>
                                            <a href="proses/proses_properti.php?id=<?=$arraySetuju['id']?>&proses=hapus">
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Hapus</button>
                                            </a>
                                        </center></td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                          </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/js/lib/data-table/datatables.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/jszip.min.js"></script>
    <script src="assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="assets/js/lib/data-table/buttons.php5.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="assets/js/lib/data-table/datatables-init.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('table.table-striped').DataTable();
        } );
    </script>

</body>
</html>
