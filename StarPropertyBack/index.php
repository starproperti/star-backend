<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Login - StarProperti</title>
  <link rel="stylesheet" type="text/css" href="assets/css/style-login.css">
</head>
<body>

  <div class="wrapper">
	<div class="container">
		<h1>Star Properti</h1>
		
		<form class="form" method="POST" action="proses/proses_login.php">
			<input type="text" placeholder="Username" name="username">
			<input type="password" placeholder="Password" name="password">
			<button name="login" type="submit" id="login-button">Login</button>
		</form>
	</div>
	
	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <!-- <script type="text/javascript">
  	 $("#login-button").click(function(event){
		event.preventDefault();
	 
	 	$('form').fadeOut(500);
		$('.wrapper').addClass('form-success');
	});
  </script> -->
</body>

</html>