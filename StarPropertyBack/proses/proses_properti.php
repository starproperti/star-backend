<?php
	session_start();
	include 'databaseUtility.php';

	if (isset($_POST['save'])) {
		$agenProperti = $_POST['agen'];
		$jenisProperti = $_POST['jenis_properti'];
		$namaProperti = $_POST['nama_properti'];
		$alamatProperti = $_POST['alamat_properti'];
		$luasProperti = $_POST['luas_properti'];
		$jumlahKamar = $_POST['jumlah_kamar'];
		$kamarMandi = $_POST['kamar_mandi'];
		$jmlKamarMandiDalam = $_POST['jml_km_dalam'];
		$jmlKamarMandiLuar = $_POST['jml_km_luar'];
		$jumlahGarasi = $_POST['jumlah_garasi'];
		$deskripsiProperti = $_POST['deskripsi_properti'];
		$hargaProperti = $_POST['harga_properti'];
		$jenisPenjualan = $_POST['jenis_penjualan'];
		$statusPenjualan = $_POST['status_penjualan'];
		$persetujuanProperti = $_POST['persetujuan'];
		$added = $_SESSION['id'];

		$thumb = "";
		$thumb = array();
		for ($i=0; $i < count($_FILES["gambar"]["name"]); $i++) { 
			$tmp = $_FILES["gambar"]["tmp_name"][$i];
			$image = $_FILES["gambar"]["name"][$i];
			$thumb[] = $image;
		}
		
		$QueryI = mysql_query("INSERT INTO data_property(id_user, id_jenis_property, nama_property, alamat_property, luas_property, jumlah_kamar, id_kamar_mandi, kamar_mandi_dalam, kamar_mandi_luar, garasi, deskripsi_property, harga_property, id_penjualan, total_lihat, id_status_property, id_status_persetujuan_property, thumbnail ,ditambah_oleh) VALUES ('$agenProperti' , '$jenisProperti' , '$namaProperti' , '$alamatProperti' , '$luasProperti' , '$jumlahKamar' , '$kamarMandi' , '$jmlKamarMandiDalam' , '$jmlKamarMandiLuar' , '$jumlahGarasi' , '$deskripsiProperti' , '$hargaProperti' , '$jenisPenjualan' , '0' , '$statusPenjualan' , '$persetujuanProperti', '$thumb[0]' ,'$added' )");

		$lastID = mysql_insert_id();
		for ($i=0; $i < count($_FILES["gambar"]["name"]); $i++) { 
			$tmp = $_FILES["gambar"]["tmp_name"][$i];
			$image = $_FILES["gambar"]["name"][$i];

			move_uploaded_file($tmp, "../tempUpload/properti/".$image);
			mysql_query("INSERT INTO foto_property(id_property , foto) VALUES ('$lastID' , '$image')");
		}
		header('location:../tambah_properti.php');
	}

	if (isset($_POST['update'])) {
		$id = $_POST['id'];
		$agenProperti = $_POST['agen'];
		$jenisProperti = $_POST['jenis_properti'];
		$namaProperti = $_POST['nama_properti'];
		$alamatProperti = $_POST['alamat_properti'];
		$luasProperti = $_POST['luas_properti'];
		$jumlahKamar = $_POST['jumlah_kamar'];
		$kamarMandi = $_POST['kamar_mandi'];
		$deskripsiProperti = $_POST['deskripsi_properti'];
		$hargaProperti = $_POST['harga_properti'];
		$jenisPenjualan = $_POST['jenis_penjualan'];
		$statusPenjualan = $_POST['status_penjualan'];
		$persetujuanProperti = $_POST['persetujuan'];
		
		for ($i=0; $i < count($_FILES["gambar"]["name"]); $i++) { 
			$tmp = $_FILES["gambar"]["tmp_name"][$i];
			$image = $_FILES["gambar"]["name"][$i];
			$moved = move_uploaded_file($tmp, "../tempUpload/properti/".$image);
			if ($moved) {
				$QueryU = mysql_query("UPDATE data_property SET id_user='$agenProperti' , id_jenis_property='$jenisProperti' , nama_property='$namaProperti' , alamat_property='$alamatProperti' , luas_property='$luasProperti' , jumlah_kamar='$jumlahKamar' , id_kamar_mandi='$kamarMandi' , deskripsi_property='$deskripsiProperti' , harga_property='$hargaProperti' , id_penjualan='$jenisPenjualan' , id_status_property='$statusPenjualan' , id_status_persetujuan_property='$persetujuanProperti' WHERE id='$id' ");
				$QueryI = mysql_query("INSERT INTO foto_property(id_property , foto) VALUES ('$id' , '$image')");
			} else {
				$QueryUp = mysql_query("UPDATE data_property SET id_user='$agenProperti' , id_jenis_property='$jenisProperti' , nama_property='$namaProperti' , alamat_property='$alamatProperti' , luas_property='$luasProperti' , jumlah_kamar='$jumlahKamar' , id_kamar_mandi='$kamarMandi' , deskripsi_property='$deskripsiProperti' , harga_property='$hargaProperti' , id_penjualan='$jenisPenjualan' , id_status_property='$statusPenjualan' , id_status_persetujuan_property='$persetujuanProperti' WHERE id='$id' ");
			}
		}
		header('location:../data_properti.php');
	}

	if (isset($_POST['tanggapi'])) {
		$id = $_POST['id'];
		$agen = $_POST['agen'];
		$persetujuan = $_POST['persetujuan'];

		$QueryU = mysql_query("UPDATE data_property SET id_user='$agen' , id_status_persetujuan_property='$persetujuan' WHERE id='$id' ");
		header("location:../data_properti.php");

	}

	if (isset($_GET['proses'])) {
		if ($_GET['proses']=='hapusgambar') {
			$id = $_GET['id'];
			$idp = $_GET['idp'];
			mysql_query("DELETE FROM foto_property WHERE id='$id' ");
			header('location:../edit_properti.php?id='.$idp);
		}
	}

	if (isset($_GET['proses'])) {
		if ($_GET['proses']=='hapus') {
			$id = $_GET['id'];
			mysql_query("DELETE FROM data_property WHERE id='$id' ");
			header('location:../data_properti.php');
		}
	}

?>