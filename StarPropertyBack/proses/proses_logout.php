<?php
	session_start();
	include 'databaseUtility.php';

	if (session_destroy()) {
		unset($_SESSION['id_level']);
		unset($_SESSION['id']);

		header('location:../');
	}
?>