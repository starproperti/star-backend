<?php
    session_start();
    include 'proses/databaseUtility.php';
    $id = $_GET['id'];
    $SelectData = mysql_query("SELECT * FROM data_property WHERE id='$id' ");
    $arrayData = mysql_fetch_array($SelectData);
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>StarProperti Admin - Dashboard</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">
    <link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.4/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.4/css/froala_style.min.css" rel="stylesheet" type="text/css" />

</head>
<style type="text/css">
    .form-group:after{
        content: "";
        clear: both;
        display: table;
    }
    .prev-img{
        margin-left: 3.5%;
    }
    .prev-img:after{
        content: "";
        clear: both;
        display: table;
    }
    .filess{
        overflow: hidden;
    }
</style>
<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <?php
            include 'sidebar.php';
        ?>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">
            <?php 
                include 'navbar.php';
            ?>
        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Properti</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Properti</a></li>
                            <li><a href="#">Data Properti</a></li>
                            <li class="active">Edit Properti</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Edit Properti</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3 class="text-center">Properti</h3>
                                    </div>
                                    <hr>
                                    <form action="proses/proses_properti.php" method="post" enctype="multipart/form-data">
                                        <div class="form-group text-center">
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-dropbox fa-2x"></i></li>
                                            </ul>
                                        </div>
                                        <!-- input -->
                                        <div class="form-group has-success">
                                            <div class="col-sm-6">
                                                <label for="cc-name" class="control-label mb-1">Agen</label>
                                                <select class="form-control" name="agen">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM user WHERE id_level = '2' ");
                                                    while ($arrayAgen = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arrayAgen['id']?>" <?php if($arrayAgen['id']==$arrayData['id_user']){echo "selected";} ?>><?=$arrayAgen['nama']." - ".$arrayAgen['no_telepon']?></option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="cc-name" class="control-label mb-1">Jenis Properti</label>
                                                <select class="form-control" name="jenis_properti">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM jenis_property");
                                                    while ($arrayProperti = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arrayProperti['id']?>" <?php if($arrayProperti['id']==$arrayData['id_jenis_property']){echo "selected";} ?>><?=$arrayProperti['jenis_property']?></option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Nama Properti</label>
                                                <input type="text" class="form-control" name="nama_properti" value="<?=$arrayData['nama_property']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Alamat Properti</label>
                                                <input type="text" class="form-control" name="alamat_properti" value="<?=$arrayData['alamat_property']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-4">
                                                <label for="cc-name" class="control-label mb-1">Luas Properti</label>
                                                <span class="input-group">
                                                    <input type="text" name="luas_properti" onkeypress='return event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46' value="<?=$arrayData['luas_property']?>" class="form-control" style="border-radius: 5px;">&nbsp;<span>m<sup>2</sup></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="cc-name" class="control-label mb-1">Jumlah Kamar</label>
                                                <span class="input-group">
                                                    <input type="text" name="jumlah_kamar" onkeypress='return event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46' value="<?=$arrayData['jumlah_kamar']?>" class="form-control" style="border-radius: 5px;">&nbsp;<span>Kamar</span>
                                                </span>
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="cc-name" class="control-label mb-1">Tipe Kamar Mandi</label>
                                                <select class="form-control" name="kamar_mandi">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM jenis_kamar_mandi");
                                                    while ($arraykm = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arraykm['id']?>" <?php if($arraykm['id']==$arrayData['id_kamar_mandi']){echo "selected";} ?>><?=$arraykm['tipe']?></option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Deskripsi Properti</label>
                                                <textarea class="form-control" name="deskripsi_properti" rows="5"><?=$arrayData['deskripsi_property']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Harga Properti</label>
                                                <span class="input-group">
                                                    <span>Rp.</span>&nbsp;<input name="harga_properti" value="<?=$arrayData['harga_property']?>" type="text" class="form-control myForm" style="border-radius: 5px;">&nbsp;
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-4">
                                                <label for="cc-name" class="control-label mb-1">Opsi Penjualan</label>
                                                <select class="form-control" name="jenis_penjualan">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM jenis_penjualan");
                                                    while ($arrayPenjualan = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arrayPenjualan['id']?>" <?php if($arrayPenjualan['id']==$arrayData['id_penjualan']){echo "selected";} ?>><?=$arrayPenjualan['tipe_penjualan']?></option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="cc-name" class="control-label mb-1">Status Penjualan</label>
                                                <select class="form-control" name="status_penjualan">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM status_property");
                                                    while ($arrayProperti = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arrayProperti['id']?>" <?php if($arrayProperti['id']==$arrayData['id_status_property']){echo "selected";} ?>><?=$arrayProperti['status']?></option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="cc-name" class="control-label mb-1">Status Persetujuan Properti</label>
                                                <select class="form-control" name="persetujuan">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM status_persetujuan_property");
                                                    while ($arraySetuju = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arraySetuju['id']?>" <?php if($arraySetuju['id']==$arrayData['id_status_persetujuan_property']){echo "selected";} ?>><?=$arraySetuju['status']?></option>
                                                <?php 
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label class="control-label mb-1">Gambar Properti</label><br>
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $no = 1;
                                                    $QueryImage = mysql_query("SELECT * FROM foto_property WHERE id_property='$arrayData[id]'");
                                                    while ($image = mysql_fetch_array($QueryImage)) {
                                                ?>
                                                <div class="col-sm-3 form-group">
                                                    <img src="tempUpload/properti/<?=$image['foto']?>" style="width: 100%; height: 180px;" alt="image_property-<?=$no++?>">
                                                    <a href="proses/proses_properti.php?id=<?=$image['id']?>&idp=<?=$arrayData['id']?>&proses=hapusgambar" class="btn btn-danger btn-block">
                                                        <i class="fa fa-trash"></i>&nbsp;
                                                        <span id="payment-button-amount">Hapus</span>
                                                    </a>
                                                </div>
                                                <?php 
                                                    }
                                                ?>
                                                <input type="file" class="form-control" id="image" name="gambar[]" multiple>
                                            </div>
                                        </div>
                                        <!-- end input -->
                                        <div>
                                            <input type="hidden" name="id" value="<?=$arrayData['id']?>">
                                            <button id="payment-button" name="update" type="submit" class="btn btn-lg btn-info btn-block">
                                                <i class="fa fa-save fa-lg"></i>&nbsp;
                                                <span id="payment-button-amount">Perbarui</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                    </div><!--/.col-->  
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.4/js/froala_editor.pkgd.min.js"></script>
    <script> 
       $(function() { 
        // $('textarea').froalaEditor({
        //     toolbarButtons: ['undo', 'redo' , '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable'],
        //     toolbarButtonsXS: ['undo', 'redo' , '-', 'bold', 'italic', 'underline']  
        // })
        $('textarea').froalaEditor({
            toolbarButtons: ['fullscreen', '|', 'bold', 'italic', 'underline' , 'strikeThrough', 'subscript', 'superscript', 'fontSize', 'color', 'align', 'orderedList', 'unorderedList', 'outdent', 'indent', 'insertTable','|' ,'undo', 'redo']
        })
    });
    </script>
    <script type="text/javascript">
        $('.myNumber').keypress(function(event) {
            if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                return true;
            else if((event.which != 46 || e.keyCode==13) && (event.which < 48 || event.which > 57))
                event.preventDefault();
        });
        $('.myNotPoint').keypress(function(event) {
            if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) 
                return true;
            else if((event.which != 46) && (event.which < 48 || event.which > 57))
                event.preventDefault();
        });
    </script>

</body>
</html>
