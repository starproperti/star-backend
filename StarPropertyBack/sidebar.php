<nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./">StarProperti</a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="dashboard.php"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <h3 class="menu-title">Properti</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Data Properti</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="tambah_properti.php">Tambah Properti</a></li>
                            <li><i class="fa fa-th"></i><a href="data_properti.php">Data Properti</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="kategori_kamar_mandi.php"> <i class="menu-icon fa fa-list"></i>Kategori Kamar Mandi </a>
                    </li>
                    <li>
                        <a href="kategori_penjualan.php"> <i class="menu-icon fa fa-list-alt"></i>Kategori Penjualan </a>
                    </li>

                    <h3 class="menu-title">Agen</h3><!-- /.menu-title -->

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Agen</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-id-badge"></i><a href="request_agen.php">Request Agen</a></li>
                            <li><i class="menu-icon fa fa-id-card-o"></i><a href="data_agen.php">Data Agen</a></li>
                        </ul>
                    </li>
                    
                    <h3 class="menu-title">Akun</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Pengguna</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a href="tambah_pengguna.php">Tambah Pengguna</a></li>
                            <li><i class="menu-icon fa fa-sitemap"></i><a href="data_pengguna.php">Data Pengguna</a></li>
                            <li><i class="menu-icon fa fa-gear"></i><a href="pengaturan_akun.php">Pengaturan Akun</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>