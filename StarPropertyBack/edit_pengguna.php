<?php
    session_start();
    include 'proses/databaseUtility.php';
    $id = $_GET['id'];
    $QueryS = mysql_query("SELECT * FROM user WHERE id='$id' ");
    $array = mysql_fetch_array($QueryS);
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>StarProperti Admin - Dashboard</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">
    <link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<style type="text/css">
    .form-group:after{
        content: "";
        clear: both;
        display: table;
    }
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 100%;
    }
    .prev-img{
        height: 100%;
    }
</style>
<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <?php
            include 'sidebar.php';
        ?>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">
            <?php 
                include 'navbar.php';
            ?>
        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengguna</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Akun</a></li>
                            <li><a href="#">Pengguna</a></li>
                            <li class="active">Tambah Pengguna</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-7">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Bio</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3 class="text-center">Biodata</h3>
                                    </div>
                                    <hr>
                                    <form action="proses/proses_pengguna.php" enctype="multipart/form-data" method="post">
                                        <div class="form-group text-center">
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-id-card-o fa-2x"></i></li>
                                            </ul>
                                        </div>
                                        <!-- input -->
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Nama Lengkap</label>
                                                <input type="text" class="form-control" name="nama" value="<?=$array['nama']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-6">
                                                <label for="cc-name" class="control-label mb-1">Jenis Kelamin</label><br>
                                                <input type="radio" name="gender" id="male" value="1" <?php if ($array['jenis_kelamin']=="1"){echo "checked";} ?>>
                                                <label for="male">
                                                    <span>Laki-laki</span>
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label></label><br>
                                                <input type="radio" name="gender" id="female" value="2" <?php if ($array['jenis_kelamin']=="2"){echo "checked";}?> >
                                                <label for="female">
                                                    <span>Perempuan</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Alamat</label>
                                                <input type="text" class="form-control" name="alamat" value="<?=$array['alamat']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">No Telepon</label>
                                                <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="no_telepon" value="<?=$array['no_telepon']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">No KTP</label>
                                                <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="no_ktp" value="<?=$array['no_ktp']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Email</label>
                                                <input type="email" class="form-control" name="email" value="<?=$array['email']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Username</label>
                                                <input type="text" class="form-control" name="username" value="<?=$array['username']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-6">
                                                <label for="cc-name" class="control-label mb-1">Pertanyaan Kemanan</label>
                                                <!-- <input type="text" class="form-control"> -->
                                                <select class="form-control" name="pertanyaan">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM jenis_pertanyaan_keamanan");
                                                    while ($arrayPertanyaan = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arrayPertanyaan['id']?>" <?php if($arrayPertanyaan['id']==$array['id_pertanyaan_keamanan']){echo "selected";}?> ><?php echo $arrayPertanyaan["pertanyaan"]; ?></option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="cc-name" class="control-label mb-1">Jawaban Keamanan</label>
                                                <input type="text" class="form-control" name="jawaban" value="<?=$array['jawaban_keamanan']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Jabatan</label>
                                                <select class="form-control" name="jabatan">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM user_level");
                                                    while ($arrayLevel = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arrayLevel['id']?>" <?php if($arrayLevel['id']==$array['id_level']){echo "selected";} ?> ><?php echo $arrayLevel["level"]; ?></option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Status Akun</label>
                                                <select class="form-control" name="status_user">
                                                <?php
                                                    include 'proses/databaseUtility.php';
                                                    $QueryS = mysql_query("SELECT * FROM status_user");
                                                    while ($arrayStatus = mysql_fetch_array($QueryS)) {
                                                ?>
                                                    <option value="<?=$arrayStatus['id']?>" <?php if($arrayStatus['id']==$array['id_status_user']){echo "selected";} ?> ><?php echo $arrayStatus["status"]; ?></option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- end input -->
                                        <div>
                                            <input type="hidden" name="id" value="<?=$id?>">
                                            <button name="editBio" id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                <i class="fa fa-save fa-lg"></i>&nbsp;
                                                <span id="payment-button-amount">Perbarui</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                    </div><!--/.col-->

                    <div class="col-lg-5">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><strong>Foto</strong></div>
                            <form action="proses/proses_pengguna.php" enctype="multipart/form-data" method="post">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <div class="form-group col-lg-12" style="width: 100%; height: 320px;">
                                        <img src="tempUpload/<?=$array['foto']?>" id="img-upload" class="prev-img"><br><br>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Browse… <input type="file" id="imgInp" name="gambar">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly>
                                    </div>
                                </div>
                                <div>
                                    <input type="hidden" name="id" value="<?=$id?>">
                                    <button name="editGambar" id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-save fa-lg"></i>&nbsp;
                                        <span id="payment-button-amount">Perbarui</span>
                                    </button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header"><strong>Password</strong></div>
                            <form action="proses/proses_pengguna.php" method="post">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <div class="form-group has-success">
                                        <div class="col-sm-12">
                                            <label for="cc-name" class="control-label mb-1">Password Baru</label>
                                            <input type="password" class="form-control" name="new_password">
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <div class="col-sm-12">
                                            <label for="cc-name" class="control-label mb-1">Konfirmasi Password Baru</label>
                                            <input type="password" class="form-control" name="kon_password">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <input type="hidden" name="id" value="<?=$id?>">
                                    <button name="editPassword" id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-save fa-lg"></i>&nbsp;
                                        <span id="payment-button-amount">Perbarui</span>
                                    </button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>  
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     
    });
</script>
</body>
</html>
