<?php
    session_start();
    include 'proses/databaseUtility.php';
    $SelectAkun = mysql_query("SELECT * FROM user WHERE id='$_SESSION[id]' ");
    $DataAkun = mysql_fetch_array($SelectAkun);
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>StarProperti Admin - Dashboard</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">
    <link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<style type="text/css">
    .form-group:after{
        content: "";
        clear: both;
        display: table;
    }
    .prev-img{
        margin-left: 3.5%;
    }
    .prev-img:after{
        content: "";
        clear: both;
        display: table;
    }
    .filess{
        overflow: hidden;
    }
</style>
<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <?php
            include 'sidebar.php';
        ?>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">
            <?php 
                include 'navbar.php';
            ?>
        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Pengguna</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Akun</a></li>
                            <li><a href="#">Pengguna</a></li>
                            <li class="active">Pengaturan Akun</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-7">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Bio</strong>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3 class="text-center">Biodata</h3>
                                    </div>
                                    <hr>
                                    <form action="" method="post" novalidate="novalidate">
                                        <div class="form-group text-center">
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-id-card-o fa-2x"></i></li>
                                            </ul>
                                        </div>
                                        <!-- input -->
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Nama Lengkap</label>
                                                <input type="text" name="nama" class="form-control" value="<?=$DataAkun['nama']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-6">
                                                <label for="cc-name" class="control-label mb-1">Jenis Kelamin</label><br>
                                                <input type="radio" name="gender" id="male" value="1" <?php if($DataAkun['jenis_kelamin']=='1'){echo "checked";} ?>>
                                                <label for="male">
                                                    <span>Laki-laki</span>
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <label></label><br>
                                                <input type="radio" name="gender" id="female" value="2" <?php if($DataAkun['jenis_kelamin']=='2'){echo "checked";} ?>>
                                                <label for="female">
                                                    <span>Perempuan</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Alamat</label>
                                                <input type="text" class="form-control" name="alamat" value="<?=$DataAkun['alamat']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">No Telepon</label>
                                                <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="no_telepon"  value="<?=$DataAkun['no_telepon']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">No KTP</label>
                                                <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="no_ktp" value="<?=$DataAkun['no_ktp']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Email</label>
                                                <input type="email" class="form-control" name="email"   value="<?=$DataAkun['email']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Username</label>
                                                <input type="text" class="form-control" name="username"   value="<?=$DataAkun['username']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-6">
                                                <label for="cc-name" class="control-label mb-1">Pertanyaan Kemanan</label>
                                                <!-- <input type="text" class="form-control"> -->
                                                <select class="form-control" name="pertanyaan">
                                                    <?php 
                                                        include 'proses/databaseUtility.php';
                                                        $QueryPertanyaan = mysql_query("SELECT * FROM jenis_pertanyaan_keamanan");
                                                        while ($quest = mysql_fetch_array($QueryPertanyaan)) {
                                                    ?>
                                                    <option value="<?=$quest['id']?>" <?php if($quest['id']==$DataAkun['id_pertanyaan_keamanan']){echo "selected";} ?>><?=$quest['pertanyaan']?></option>
                                                    <?php
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="cc-name" class="control-label mb-1">Jawaban Keamanan</label>
                                                <input type="text" class="form-control" name="jawaban" value="<?=$DataAkun['jawaban_keamanan']?>">
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <div class="col-sm-12">
                                                <label for="cc-name" class="control-label mb-1">Jabatan</label>
                                                <input type="text" class="form-control" value="<?php if($DataAkun['id_level']==1){echo "Admin";} ?>"  disabled="true">
                                            </div>
                                        </div>
                                        <!-- end input -->
                                        <div>
                                            <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                <i class="fa fa-save fa-lg"></i>&nbsp;
                                                <span id="payment-button-amount">Simpan</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                    </div><!--/.col-->
                  <div class="col-lg-5">
                    <div class="card">
                      <div class="card-header"><strong>Foto</strong></div>
                      <div class="card-body card-block">
                        <div class="form-group">
                            <img src="tempUpload/<?=$DataAkun['foto']?>" class="prev-img"><br><br>
                            <input type="file" id="company" placeholder="Enter your company name" class="form-control filess">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>  
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>
