<?php
	class Properti{
		private $conn;

		public $id;
		public $idUser;
		public $idJenisProperti;
		public $namaProperti;
		public $alamatProperti;
		public $luasProperti;
		public $jumlahKamar;
		public $idKamarMandi;
		public $kamarMandiDalam;
		public $kamarMandiLuar;
		public $garasi;
		public $deskripsiProperti;
		public $hargaProperti;
		public $idPenjualan;
		public $totalLihat;
		public $idStatusProperti;
		public $idPersetujuanProperti;
		public $fotoProperti;
		public $addedBy;

		public $namaUser;
		public $jenisKelamin;
		public $alamat;
		public $noTelepon;
		public $email;
		public $foto;

		public $allFoto;
		public $singleFoto;

		public function __construct($db){
			$this->conn = $db;
		}

		//Read All (Belum Terjual)
		public function readNotSold() {
			$QueryS = 'SELECT * FROM data_property WHERE data_property.id_status_property = 2';
			$stmt = $this->conn->prepare($QueryS);
			$stmt->execute();
			return $stmt;
		}
		public function readNotSoldLimit() {
			$QueryS = 'SELECT * FROM data_property WHERE data_property.id_status_property = 2 ORDER BY id DESC LIMIT 3';
			$stmt = $this->conn->prepare($QueryS);
			$stmt->execute();
			return $stmt;
		}

		//Read All (Terjual)
		public function readSoldOut() {
			$QueryS = 'SELECT * FROM data_property WHERE data_property.id_status_property = 1 ';
			$stmt = $this->conn->prepare($QueryS);
			$stmt->execute();
			return $stmt;
		}
		public function raedSoldOutLimit() {
			$QueryS = 'SELECT * FROM data_property WHERE data_property.id_status_property = 1 ORDER BY id DESC LIMIT 3';
			$stmt = $this->conn->prepare($QueryS);
			$stmt->execute();
			return $stmt;
		}

		//Read Single Property
		public function readSingleProperty() {
			$QuerySe = 'SELECT data_property.id, data_property.id_user, data_property.id_jenis_property, data_property.nama_property, data_property.alamat_property, data_property.luas_property, data_property.jumlah_kamar, data_property.id_kamar_mandi, data_property.kamar_mandi_dalam, data_property.kamar_mandi_luar, data_property.garasi, data_property.deskripsi_property, data_property.harga_property, data_property.id_penjualan, data_property.total_lihat, data_property.id_status_property, data_property.id_status_persetujuan_property, data_property.ditambah_oleh, user.nama, user.jenis_kelamin, user.alamat, user.no_telepon, user.email, user.foto  FROM data_property  INNER JOIN user ON data_property.id_user = user.id WHERE data_property.id = ?';
			$stmt = $this->conn->prepare($QuerySe);
			$stmt->bindParam(1, $this->id);
			$stmt->execute();
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			$this->idUser = $row['id_user'];
			$this->idJenisProperti = $row['id_jenis_property'];
			$this->namaProperti = $row['nama_property'];
			$this->alamatProperti = $row['alamat_property'];
			$this->luasProperti = $row['luas_property'];
			$this->jumlahKamar = $row['jumlah_kamar'];
			$this->idKamarMandi = $row['id_kamar_mandi'];
			$this->kamarMandiDalam = $row['kamar_mandi_dalam'];
			$this->kamarMandiLuar = $row['kamar_mandi_luar'];
			$this->garasi = $row['garasi'];
			$this->deskripsiProperti = $row['deskripsi_property'];
			$this->hargaProperti = $row['harga_property'];
			$this->idPenjualan = $row['id_penjualan'];
			$this->totalLihat = $row['total_lihat'];
			$this->idStatusProperti = $row['id_status_property'];
			$this->idPersetujuanProperti = $row['id_status_persetujuan_property'];
			$this->addedBy = $row['ditambah_oleh'];

			$this->namaUser = $row['nama'];
			$this->jenisKelamin = $row['jenis_kelamin'];
			$this->alamat = $row['alamat'];
			$this->noTelepon = $row['no_telepon'];
			$this->email = $row['email'];
			$this->foto = $row['foto'];

		}

		//Read Single Image All
		public function readSingleAllImage() {
			$QueryS = 'SELECT * FROM data_property, foto_property where data_property.id=foto_property.id_property and data_property.id = ? ';
			$stmt = $this->conn->prepare($QueryS);
			$stmt->bindParam(1, $this->id);
			$stmt->execute();
			return $stmt;
		}
	}
 ?>