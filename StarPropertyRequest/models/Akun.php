<?php
	class Akun{
		private $conn;

		public $id;
		public $nama;
		public $jenisKelamin;
		public $alamat;
		public $noTelepon;
		public $noKtp;
		public $email;
		public $username;
		public $password;
		public $idPertanyaanKeamanan;
		public $jawabanKeamanan;
		public $idStatusUser;
		public $foto;
		public $idLevel;

		public function __construct($db){
			$this->conn = $db;
		}

		//Daftar
		public function register(){
			$QueryI = 'INSERT INTO user SET nama=:nama, email=:email, username=:username, password=:password, id_pertanyaan_keamanan=:idPertanyaanKeamanan, jawaban_keamanan=:jawabanKeamanan , id_status_user="3", id_level="3" ';
			$stmt = $this->conn->prepare($QueryI);
			$this->nama = htmlspecialchars(strip_tags($this->nama));
			$this->email = htmlspecialchars(strip_tags($this->email));
			$this->username = htmlspecialchars(strip_tags($this->username));
			$this->password = htmlspecialchars(strip_tags($this->password));
			$this->jawabanKeamanan = htmlspecialchars(strip_tags($this->jawabanKeamanan));

			$stmt->bindParam(':nama', $this->nama);
			$stmt->bindParam(':email', $this->email);
			$stmt->bindParam(':username', $this->username);
			$stmt->bindParam(':password', $this->password);
			$stmt->bindParam(':idPertanyaanKeamanan', $this->idPertanyaanKeamanan);
			$stmt->bindParam(':jawabanKeamanan', $this->jawabanKeamanan);
			if ($stmt->execute()) {
				return true;
			}
			printf("Error: %s.\n", $stmt->error);
			return false;
		}

		//Login
		public function logins() {
			$QueryS = 'SELECT * FROM user WHERE username=:username AND password=:password';
			$stmt = $this->conn->prepare($QueryS);
			$this->username = htmlspecialchars(strip_tags($this->username));
			$this->password = htmlspecialchars(strip_tags($this->password));

			$stmt->bindParam(':username', $this->username);
			$stmt->bindParam(':password', $this->password);
			$stmt->execute();
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			if ($stmt->rowCount() > 0) {
				$this->id = $row['id'] ;
				$this->nama = $row['nama'] ;
				$this->jenisKelamin = $row['jenis_kelamin'] ;
				$this->alamat = $row['alamat'] ;
				$this->noTelepon = $row['no_telepon'] ;
				$this->noKtp = $row['no_ktp'] ;
				$this->email = $row['email'] ;
				$this->username = $row['username'] ;
				$this->password = $row['password'] ;
				$this->idPertanyaanKeamanan = $row['id_pertanyaan_keamanan'] ;
				$this->jawabanKeamanan = $row['jawaban_keamanan'] ;
				$this->idStatusUser = $row['id_status_user'] ;
				$this->foto = $row['foto'] ;
				$this->idLevel = $row['id_level'] ;
			} else {
				echo "Data Tidak Ada";
			}
		}
	}
?>