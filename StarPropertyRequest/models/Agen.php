<?php
	class Agen{
		private $conn;

		public $id;
		public $nama;
		public $jenisKelamin;
		public $alamat;
		public $noTelepon;
		public $noKtp;
		public $email;
		public $username;
		public $password;
		public $idPertanyaanKeamanan;
		public $jawabanKeamanan;
		public $idStatusUser;
		public $foto;
		public $idLevel;

		public function __construct($db){
			$this->conn = $db;
		}
		
		//All Agen
		public function readAllAgen(){
			$QueryS = "SELECT * FROM user WHERE id_level = 2";
			$stmt = $this->conn->prepare($QueryS);
			$stmt->execute();
			return $stmt;
		}
	}
?>