<?php 
	class Pertanyaan{
		private $conn;

		public $id;
		public $pertanyaan;

		public function __construct($db){
			$this->conn = $db;
		}

		public function readPertanyaan(){
			$QueryS = "SELECT * FROM jenis_pertanyaan_keamanan";
			$stmt = $this->conn->prepare($QueryS);
			$stmt->execute();
			return $stmt;
		}
	}	
?>