<?php
	//Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../utility/databaseUtility.php';
	include_once '../../models/Agen.php';

	$database = new databaseUtility();
	$db = $database->connect();

	$post = new Agen($db);
	$result = $post->readAllAgen();
	$num = $result->rowCount();

	if ($num > 0) {
		$post_arr = array();
		$post_arr['data'] = array();

		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			extract($row);
			$post_item = array(
				'id' => $id,
				'nama' => $nama,
				'jenisKelamin' => $jenis_kelamin,
				'alamat' => $alamat,
				'noTelepon' => $no_telepon,
				'noKtp' => $no_ktp,
				'email' => $email,
				// 'username' => $username,
				// 'password' => $password,
				// 'idPertanyaanKeamanan' => $id_pertanyaan_keamanan,
				// 'jawabanKeamanan' => $jawaban_keamanan,
				'idStatusUser' => $id_status_user,
				'foto' => $foto,
				'idLevel' => $id_level
			);
			array_push($post_arr['data'], $post_item);
		}
		echo json_encode($post_arr);
	} else {
		echo json_encode(
			array('message' => 'No Data Found')
		);
	}
?>