<?php 
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: multipart/form-data");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With");

	include_once '../../utility/databaseUtility.php';
	include_once '../../models/Akun.php';

	$database = new databaseUtility();
	$db = $database->connect();

	$post = new Akun($db);

	$data = json_decode(file_get_contents("php://input"));
	/*$post->nama = $data->nama;
	$post->email = $data->email;
	$post->username = $data->username;
	$post->password = $data->password;
	$post->idPertanyaanKeamanan = $data->idPertanyaanKeamanan;
	$post->jawabanKeamanan = $data->jawabanKeamanan;*/

	$post->nama = $_POST['nama'];
	$post->email = $_POST['email'];
	$post->username = $_POST['username'];
	$post->password = $_POST['password'];
	$post->idPertanyaanKeamanan = $_POST['idPertanyaanKeamanan'];
	$post->jawabanKeamanan = $_POST['jawabanKeamanan'];

	if($post->register()){
		echo json_encode(
			array('message' => 'Post Success')
		);
	} else {
		echo json_encode(
			array('message' => 'Post Failed')
		);
	}
?>