<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json'); 

	/*header("Access-Control-Allow-Origin: *");
	header("Content-Type: multipart/form-data");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With");*/

	include_once '../../utility/databaseUtility.php';
	include_once '../../models/Akun.php';

	$database = new databaseUtility();
	$db = $database->connect();

	$login = new Akun($db);

	$data = json_decode(file_get_contents("php://input"));
	$login->username = $data->username;
	$login->password = $data->password;

	/*$login->username = $_POST['username'];
	$login->password = $_POST['password'];*/

	$login->logins();
	$post_item['data'] = array();
	$post_arr = array(
		'id' => $login->id,
		'nama' => $login->nama,
		'jenisKelamin' => $login->jenisKelamin,
		'alamat' => $login->alamat,
		'noTelepon' => $login->noTelepon,
		'noKtp' => $login->noKtp,
		'email' => $login->email,
		'username' => $login->username,
		'password' => $login->password,
		'idPertanyaanKeamanan' => $login->idPertanyaanKeamanan,
		'jawabanKeamanan' => $login->jawabanKeamanan,
		'idStatusUser' => $login->idStatusUser,
		'foto' => $login->foto,
		'idLevel' => $login->idLevel
	);
	array_push($post_item['data'], $post_arr);
	print_r(json_encode($post_item));

?>