<?php

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../utility/databaseUtility.php';
	include_once '../../models/Properti.php';

	$database = new databaseUtility();
	$db = $database->connect();

	$post = new Properti($db);
	$result = $post->readSoldOut();
	$num = $result->rowCount();

	if($num > 0){
		$posts_arr = array();
		$posts_arr['data'] = array();

		while($row = $result->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$post_item = array(
				'id' => $id,
				'idUser' => $id_user,
				'idJenisProperti' => $id_jenis_property,
				'namaProperti' => $nama_property,
				'alamatProperti' => $alamat_property,
				'luasProperti' => $luas_property,
				'jumlahKamar' => $jumlah_kamar,
				'idKamarMandi' => $id_kamar_mandi,
				'jumlahKamarMandiDalam' => $kamar_mandi_dalam,
				'jumlahKamarMandiLuar' => $kamar_mandi_luar,
				'garasi' => $garasi,
				'deskripsiProperti' => $deskripsi_property,
				'hargaProperti' => $harga_property,
				'idPenjualan' => $id_penjualan,
				'totalLihat' => $total_lihat,
				'idStatusProperti' => $id_status_property,
				'idStatusPersetujuanProperti' => $id_status_persetujuan_property,
				'thumbnail' => $thumbnail,
				'ditambahOleh' => $ditambah_oleh
			);
			array_push($posts_arr['data'], $post_item);
		}
		echo json_encode($posts_arr);
	} else {
		echo json_encode(
			array('message' => 'No Data Found')
		);
	}
?>