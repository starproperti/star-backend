<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../utility/databaseUtility.php';
	include_once '../../models/Properti.php';

	$database = new databaseUtility();
	$db = $database->connect();

	$post = new Properti($db);
	$post->id = isset($_GET['id']) ? $_GET['id'] : die();
	$post->status = isset($_GET['status']) ? $_GET['status'] : die();
	$post->readSinglePortofolio();

	$post_item['data'] = array();
	$post_arr = array(
		'id' => $post->id,
		'idUser' => $post->idUser,
		'idJenisProperti' => $post->idJenisProperti,
		'namaProperti' => $post->namaProperti,
		'alamatProperti' => $post->alamatProperti,
		'luasProperti' => $post->luasProperti,
		'jumlahKamar' => $post->jumlahKamar,
		'idKamarMandi' => $post->idKamarMandi,
		'deskripsiProperti' => $post->deskripsiProperti,
		'hargaProperti' => $post->hargaProperti,
		'idPenjualan' => $post->idPenjualan,
		'totalLihat' => $post->totalLihat,
		'idStatusProperti' => $post->idStatusProperti,
		'idStatusPersetujuanProperti' => $post->idPersetujuanProperti,
		'ditambahOleh' => $post->addedBy,
		'namaUser' => $post->namaUser,
		'jenisKelamin' => $post->jenisKelamin,
		'alamat' => $post->alamat,
		'noTelepon' => $post->noTelepon,
		'email' => $post->email,
		'foto' => $post->foto
	);
	array_push($post_item['data'], $post_arr);
	print_r(json_encode($post_item));
?>