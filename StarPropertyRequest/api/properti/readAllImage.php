<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../utility/databaseUtility.php';
	include_once '../../models/Properti.php';

	$database = new databaseUtility();
	$db = $database->connect();

	$post = new Properti($db);
	$post->id = isset($_GET['id']) ? $_GET['id'] : die();
	$result = $post->readSingleAllImage();
	$num = $result->rowCount();

	if($num > 0){
		$posts_arr = array();
		$posts_arr['data'] = array();

		while($row = $result->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$post_item = array(
				'id' => $id,
				'foto' => $foto
			);
			array_push($posts_arr['data'], $post_item);
		}
		echo json_encode($posts_arr);
	} else {
		echo json_encode(
			array('message' => 'No Data Found')
		);
	}
?>