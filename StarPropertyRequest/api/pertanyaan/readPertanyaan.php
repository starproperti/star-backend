<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../utility/databaseUtility.php';
	include_once '../../models/Pertanyaan.php';

	$database = new databaseUtility();
	$db = $database->connect();

	$post = new Pertanyaan($db);
	$result = $post->readPertanyaan();
	$num = $result->rowCount();

	if($num > 0){
		$post_arr = array();
		$post_arr['data'] = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			extract($row);
			$post_item = array(
				'id' => $id,
				'pertanyaan' => $pertanyaan
			);
			array_push($post_arr['data'], $post_item);
		}
		echo json_encode($post_arr);
	} else {
		echo json_encode(
			array('message' => 'No Data Found')
		);
	}
?>